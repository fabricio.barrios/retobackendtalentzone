package com.retobackend.application.services;

import com.retobackend.application.dtos.CreateCyclistTeamDto;
import com.retobackend.application.dtos.CyclistDto;
import com.retobackend.application.dtos.CyclistTeamDto;
import com.retobackend.application.models.Country;
import com.retobackend.application.models.Cyclist;
import com.retobackend.application.models.CyclistTeam;
import com.retobackend.application.repository.CountryRepository;
import com.retobackend.application.repository.CyclistRepository;
import com.retobackend.application.repository.CyclistTeamRepository;
import com.retobackend.application.utils.AppUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static org.mockito.Mockito.when;


@SpringBootTest
class CyclistTeamServiceTest {

    @MockBean
    CyclistTeamRepository repository;

    @MockBean
    CyclistRepository cyclistRepository;

    @MockBean
    CountryRepository countryRepository;

    @Autowired
    CyclistTeamService service;

    @Test
    void findAllCyclingTeams() {
        CyclistTeam cyclistTeam1 = new CyclistTeam(
                "12",
                "TeamName",
                "123",
                new Country("1", "URU", "Uruguay"),
                new ArrayList<>(List.of(new Cyclist())));

        CyclistTeam cyclistTeam2 = new CyclistTeam(
                "22",
                "TeamName2",
                "1234",
                new Country("1", "URU", "Uruguay"),
                new ArrayList<>(List.of(new Cyclist())));

        when(repository.findAll()).thenReturn(Flux.just(cyclistTeam1, cyclistTeam2));

        StepVerifier.create(service.findAllCyclingTeams())
                .expectNextMatches(cyclistTeamDto ->
                                cyclistTeamDto.getTeamName().equals("TeamName") &&
                                cyclistTeamDto.getCode().equals("123") &&
                                cyclistTeamDto.getCountry().equals(new Country("1", "URU", "Uruguay")) &&
                                cyclistTeamDto.getCyclists().equals(cyclistTeam1.getCyclists())
                )
                .expectNextMatches(cyclistTeamDto ->
                        cyclistTeamDto.getTeamName().equals("TeamName2") &&
                                cyclistTeamDto.getCode().equals("1234") &&
                                cyclistTeamDto.getCountry().equals(new Country("1", "URU", "Uruguay")) &&
                                cyclistTeamDto.getCyclists().equals(cyclistTeam2.getCyclists())
                )
                .expectComplete()
                .verify();
    }

    @Test
    void findCyclingTeamByTeamCode() {

        CyclistTeam cyclistTeam1 = new CyclistTeam(
                "12",
                "TeamName",
                "123",
                new Country("1", "URU", "Uruguay"),
                new ArrayList<>(List.of(new Cyclist())));

        CyclistTeam cyclistTeam2 = new CyclistTeam(
                "22",
                "TeamName2",
                "1234",
                new Country("1", "URU", "Uruguay"),
                new ArrayList<>(List.of(new Cyclist())));

        when(repository.findAll()).thenReturn(Flux.just(cyclistTeam1, cyclistTeam2));

        StepVerifier.create(service.findCyclingTeamByTeamCode("123"))
                .expectNextMatches( cyclistTeamDto ->
                        cyclistTeamDto.getTeamName().equals("TeamName") &&
                                cyclistTeamDto.getCode().equals("123") &&
                                cyclistTeamDto.getCountry().equals(new Country("1", "URU", "Uruguay")) &&
                                cyclistTeamDto.getCyclists().equals(cyclistTeam1.getCyclists())
                )
                .expectComplete()
                .verify();

        StepVerifier.create(service.findCyclingTeamByTeamCode("1234"))
                .expectNextMatches( cyclistTeamDto ->
                        cyclistTeamDto.getTeamName().equals("TeamName2") &&
                                cyclistTeamDto.getCode().equals("1234") &&
                                cyclistTeamDto.getCountry().equals(new Country("1", "URU", "Uruguay")) &&
                                cyclistTeamDto.getCyclists().equals(cyclistTeam1.getCyclists())
                )
                .expectComplete()
                .verify();
    }

    @Test
    void findAllCyclistsByTeamCode() {

        CyclistTeam cyclistTeam1 = new CyclistTeam(
                "12",
                "TeamName",
                "123",
                new Country("1", "URU", "Uruguay"),
                new ArrayList<>(List.of(new Cyclist())));

        CyclistTeam cyclistTeam2 = new CyclistTeam(
                "22",
                "TeamName2",
                "1234",
                new Country("1", "URU", "Uruguay"),
                new ArrayList<>(List.of(new Cyclist())));

        CyclistTeam cyclistTeam3 = new CyclistTeam(
                "23",
                "TeamName2",
                "1234",
                new Country("1", "URU", "Uruguay"),
                new ArrayList<>(List.of(new Cyclist())));

        when(repository.findAll()).thenReturn(Flux.just(cyclistTeam1, cyclistTeam2, cyclistTeam3));

        StepVerifier.create(service.findAllCyclistsByTeamCode("1234"))
                .expectNextCount(2)
                .expectComplete()
                .verify();

        StepVerifier.create(service.findAllCyclistsByTeamCode("123"))
                .expectNextCount(1)
                .expectComplete()
                .verify();
    }

    @Test
    void addCyclistToTeamByTeamIDAndIdCylist() {

        Cyclist cyclist = new Cyclist("77", "Ricardiño Manasuela", "600", "BR");

        CyclistTeam cyclistTeam = new CyclistTeam(
                "12",
                "TeamName",
                "123",
                new Country("1", "URU", "Uruguay"),
                new ArrayList<>());

        CyclistTeam cyclistTeamUpdated = new CyclistTeam(
                "12",
                "TeamName",
                "123",
                new Country("1", "URU", "Uruguay"),
                new ArrayList<>( List.of(cyclist) ));

        when(repository.findById("12")).thenReturn(Mono.just(cyclistTeam));
        when(cyclistRepository.findById("77")).thenReturn(Mono.just(cyclist));
        when(repository.save(cyclistTeamUpdated)).thenReturn(Mono.just(cyclistTeamUpdated));

        StepVerifier.create(service.addCyclistToTeamByTeamIDAndIdCylist("12", "77"))
                .expectNextMatches(
                        cyclistTeamDto -> cyclistTeamDto.getCyclists().get(0).equals(cyclist)
                )
                .expectComplete()
                .verify();

    }

    @Test
    void saveCyclingTeam() throws ExecutionException, InterruptedException {

        Country country = new Country("1", "URU","Uruguay");

        ArrayList<CyclistDto> cyclists = new ArrayList<>(List.of());

        CreateCyclistTeamDto createCyclistTeamDto = new CreateCyclistTeamDto(
                "Los Piratas",
                "123",
                country,
                cyclists
        );

        CyclistTeam cyclistTeam = AppUtils.createCyclistTeamDtoToCyclistTeam(createCyclistTeamDto);

        when(countryRepository.findById("1")).thenReturn(Mono.just(country));
        when(repository.save(cyclistTeam)).thenReturn(Mono.just(cyclistTeam));

        StepVerifier.create(service.saveCyclingTeam(createCyclistTeamDto, "1"))
                .expectNextMatches(cyclistTeamDto ->
                                    cyclistTeamDto.getTeamName().equals("Los Piratas") &&
                                    cyclistTeamDto.getCode().equals("123") &&
                                    cyclistTeamDto.getCountry().getCode().equals(country.getCode()) &&
                                    cyclistTeamDto.getCyclists().isEmpty()
                        )
                .expectComplete()
                .verify();
    }

    @Test
    void delete() {

        Country country = new Country("1", "URU","Uruguay");
        ArrayList<CyclistDto> cyclists = new ArrayList<>(List.of());
        CreateCyclistTeamDto createCyclistTeamDto = new CreateCyclistTeamDto(
                "Los Piratas",
                "123",
                country,
                cyclists
        );

        CyclistTeam cyclistTeam = AppUtils.createCyclistTeamDtoToCyclistTeam(createCyclistTeamDto);
        cyclistTeam.setId("44");

        when(repository.findById("44")).thenReturn(Mono.just(cyclistTeam));
        when(repository.deleteById("44")).thenReturn(Mono.empty());

        StepVerifier.create(service.delete("44"))
                .expectNext()
                .expectComplete()
                .verify();
    }

    @Test
    void update() {

        Country country = new Country("1", "URU","Uruguay");
        ArrayList<CyclistDto> cyclists = new ArrayList<>(List.of());
        CreateCyclistTeamDto createCyclistTeamDto = new CreateCyclistTeamDto(
                "Los Piratas",
                "123",
                country,
                cyclists
        );

        ArrayList<Cyclist> cyclists2 = cyclists
                .stream()
                .map(AppUtils::cyclistDtoToCyclist)
                .collect(Collectors.toCollection(ArrayList::new));
        CyclistTeam cyclistTeam = AppUtils.createCyclistTeamDtoToCyclistTeam(createCyclistTeamDto);
        cyclistTeam.setId("44");
        cyclistTeam.setCyclists(cyclists2);
        cyclistTeam.setCountry(country);
        cyclistTeam.setTeamName("Las pirañitas");
        cyclistTeam.setCode("123");

        CyclistTeam cyclistTeamModified = new CyclistTeam();
        BeanUtils.copyProperties(cyclistTeam, cyclistTeamModified);
        cyclistTeamModified.setTeamName("Los tatitos");

        when(repository.findById("44")).thenReturn(Mono.just(cyclistTeam));
        when(repository.save(cyclistTeamModified)).thenReturn(Mono.just(cyclistTeam));

        StepVerifier.create(service.update(AppUtils.cyclisTeamToCyclistTeamDto(cyclistTeamModified), "44"))
                .expectNextMatches(cyclistTeamDto ->
                        cyclistTeamDto.getTeamName().equals(cyclistTeam.getTeamName()) &&
                                cyclistTeamDto.getCode().equals(cyclistTeam.getCode()) &&
                                cyclistTeamDto.getId().equals(cyclistTeam.getId())
                )
                .expectComplete()
                .verify();

    }
}