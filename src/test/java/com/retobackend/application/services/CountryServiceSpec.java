package com.retobackend.application.services;

import com.retobackend.application.dtos.CreateCountryDto;
import com.retobackend.application.models.Country;
import com.retobackend.application.repository.CountryRepository;
import com.retobackend.application.utils.AppUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.when;

@SpringBootTest
class CountryServiceSpec {

    @Autowired
    CountryService service;

    @MockBean
    CountryRepository repository;

    @Test
    void findAllCountry() {

        Country country1 = new Country("123", "URU", "Uruguay");
        Country country2 = new Country("124", "ARG", "Argentina");

        when(repository.findAll())
                .thenReturn(Flux.just(country1, country2));

        StepVerifier.create(service.findAllCountry())
                .expectNext(AppUtils.countryToCountryDto(country1))
                .expectNext(AppUtils.countryToCountryDto(country2))
                .expectComplete()
                .verify();

    }

    @Test
    void save() {

        CreateCountryDto createCountryDto = new CreateCountryDto("Uruguay", "UY");

        Country country = AppUtils.createCountryDtoToCountry(createCountryDto);

        when(repository.save(country))
                .thenReturn(Mono.just(country));

        StepVerifier.create(service.save(createCountryDto))
                .expectNextMatches(countryDto ->
                                countryDto.getCode().equals("UY") &&
                                countryDto.getName().equals("Uruguay")
                        )
                .expectComplete()
                .verify();
    }

    @Test
    void delete() {

        Country country = new Country("123", "URU", "Uruguay");

        when(repository.findById("123")).thenReturn(Mono.just(country));
        when(repository.deleteById("123")).thenReturn(Mono.empty());

        StepVerifier.create(service.delete(country.getId()))
                .expectNext()
                .expectComplete()
                .verify();
    }

    @Test
    void update() {

        Country country = new Country("123", "UY", "Urusawa");
        Country countryUpdated = new Country("123", "URU", "Uruguay");

        when(repository.save(country)).thenReturn(Mono.just(country));
        when(repository.findById("123")).thenReturn(Mono.just(country));
        when(repository.save(countryUpdated)).thenReturn(Mono.just(countryUpdated));

        StepVerifier.create(service.update(
                AppUtils.countryToCountryDto(countryUpdated),"123"))
                .expectNext(AppUtils.countryToCountryDto(countryUpdated))
                .expectComplete()
                .verify();

    }

    @Test
    void countryByCode() {

        Country country1 = new Country("123", "URU", "Uruguay");
        Country country2 = new Country("124", "ARG", "Argentina");

        when(repository.save(country1)).thenReturn(Mono.just(country1));
        when(repository.save(country2)).thenReturn(Mono.just(country2));
        when(repository.findAll()).thenReturn(Flux.just(country1, country2));

        StepVerifier.create(service.countryByCode("URU"))
                .expectNext(AppUtils.countryToCountryDto(country1))
                .expectComplete()
                .verify();

        StepVerifier.create(service.countryByCode("ARG"))
                .expectNext(AppUtils.countryToCountryDto(country2))
                .expectComplete()
                .verify();

    }
}