package com.retobackend.application.services;

import com.retobackend.application.dtos.CreateCyclistDto;
import com.retobackend.application.models.Cyclist;
import com.retobackend.application.repository.CyclistRepository;
import com.retobackend.application.utils.AppUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.Mockito.when;

@SpringBootTest
class CyclistServiceSpec {

    @MockBean
    CyclistRepository repository;

    @Autowired
    CyclistService service;

    @Test
    void save() {

        CreateCyclistDto createCyclistDto = new CreateCyclistDto("Robertiño Mañiño", "500", "URU");

        Cyclist cyclist = AppUtils.createCyclistDtoToCiclist(createCyclistDto);

        when(repository.save(cyclist)).thenReturn(Mono.just(cyclist));

        StepVerifier.create(service.save(createCyclistDto))
                .expectNextMatches(cyclistDto ->
                                cyclistDto.getFullName().equals("Robertiño Mañiño") &&
                                cyclistDto.getNumCompetitor().equals("500") &&
                                cyclistDto.getCountry().equals("URU")
                )
                .expectComplete()
                .verify();

    }

    @Test
    void findAllCyclists() {

        Cyclist cyclist1 = new Cyclist("1", "Robertiño Mañiño", "500", "URU");
        Cyclist cyclist2 = new Cyclist("2", "Caso del Miro", "300", "ARG");

        when(repository.findAll()).thenReturn(Flux.just(cyclist1, cyclist2));

        StepVerifier.create(service.findAllCyclists())
                .expectNextCount(2)
                .expectComplete()
                .verify();

    }

    @Test
    void findAllCyclistsByCountry() {

        Cyclist cyclist1 = new Cyclist("1", "Robertiño Mañiño", "500", "URU");
        Cyclist cyclist2 = new Cyclist("2", "Caso del Miro", "300", "ARG");
        Cyclist cyclist3 = new Cyclist("3", "Santiago Garrafales", "150", "URU");

        when(repository.findAll()).thenReturn(Flux.just(cyclist1, cyclist2, cyclist3));

        StepVerifier.create(service.findAllCyclistsByCountry("URU"))
                .expectNextCount(2)
                .expectComplete()
                .verify();

        StepVerifier.create(service.findAllCyclistsByCountry("ARG"))
                .expectNextCount(1)
                .expectComplete()
                .verify();
    }

    @Test
    void delete() {

        Cyclist cyclist = new Cyclist("1", "Robertiño Mañiño", "500", "URU");

        when(repository.findById("1")).thenReturn(Mono.just(cyclist));
        when(repository.deleteById("1")).thenReturn(Mono.empty());

        StepVerifier.create(service.delete("1"))
                .expectNext()
                .expectComplete()
                .verify();
    }

    @Test
    void update() {

        Cyclist cyclist = new Cyclist("1", "Robertiño Mañiño", "500", "URU");
        Cyclist cyclistUpdated = new Cyclist("1", "Robertiño Mañataro", "250", "URU");

        when(repository.findById("1")).thenReturn(Mono.just(cyclist));
        when(repository.save(cyclistUpdated)).thenReturn(Mono.just(cyclistUpdated));

        StepVerifier.create(service.update(AppUtils.cyclistToCyclistDto(cyclistUpdated), "1"))
                .expectNextMatches(cyclistDto ->
                                cyclistDto.getCountry().equals(cyclistUpdated.getCountry()) &&
                                cyclistDto.getNumCompetitor().equals(cyclistUpdated.getNumCompetitor()) &&
                                cyclistDto.getFullName().equals(cyclistUpdated.getFullName())
                )
                .expectComplete()
                .verify();
    }

    @DisplayName("JUnit test for findCyclistByCompetitorNumber method")
    @Test
    void findCyclistByCompetitorNumber() {

        Cyclist cyclist1 = new Cyclist("1", "Robertiño Mañiño", "500", "URU");
        Cyclist cyclist2 = new Cyclist("2", "Mauricio San Ramón", "250", "ARG");

        when(repository.findAll()).thenReturn(Flux.just(cyclist1, cyclist2));

        StepVerifier.create(service.findCyclistByCompetitorNumber("500"))
                .expectNextMatches(cyclistDto ->
                                cyclistDto.getNumCompetitor().equals("500") &&
                                cyclistDto.getCountry().equals("URU") &&
                                cyclistDto.getFullName().equals("Robertiño Mañiño")
                )
                .expectComplete()
                .verify();

        StepVerifier.create(service.findCyclistByCompetitorNumber("250"))
                .expectNextMatches(cyclistDto ->
                        cyclistDto.getNumCompetitor().equals("250") &&
                                cyclistDto.getCountry().equals("ARG") &&
                                cyclistDto.getFullName().equals("Mauricio San Ramón")
                )
                .expectComplete()
                .verify();
    }
}