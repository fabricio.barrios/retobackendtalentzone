package com.retobackend.application.utils;

import com.retobackend.application.dtos.*;
import com.retobackend.application.models.Country;
import com.retobackend.application.models.Cyclist;
import com.retobackend.application.models.CyclistTeam;
import org.springframework.beans.BeanUtils;

public class AppUtils {

    // MODEL TO DTO

    public static CyclistDto cyclistToCyclistDto(Cyclist cyclist) {
        CyclistDto cyclistDto = new CyclistDto();
        BeanUtils.copyProperties(cyclist, cyclistDto);
        return cyclistDto;
    }

    public static CyclistTeamDto cyclisTeamToCyclistTeamDto(CyclistTeam cyclistTeam) {
        CyclistTeamDto cyclistTeamDto = new CyclistTeamDto();
        BeanUtils.copyProperties(cyclistTeam, cyclistTeamDto);
        return cyclistTeamDto;
    }

    public static CountryDto countryToCountryDto(Country country) {
        CountryDto countryDto = new CountryDto();
        BeanUtils.copyProperties(country, countryDto);
        return countryDto;
    }


    // DTO TO MODEL

    public static Cyclist cyclistDtoToCyclist(CyclistDto cyclistDto) {
        Cyclist cyclist = new Cyclist();
        BeanUtils.copyProperties(cyclistDto, cyclist);
        return cyclist;
    }

    public static CyclistTeam cyclistTeamDtoToCyclistTeam(CyclistTeamDto cyclistTeamDto) {
        CyclistTeam cyclistTeam = new CyclistTeam();
        BeanUtils.copyProperties(cyclistTeamDto, cyclistTeam);
        return cyclistTeam;
    }

    public static Country countryDtoToCountry(CountryDto countryDto) {
        Country country = new Country();
        BeanUtils.copyProperties(countryDto, country);
        return country;
    }

    // CREATE_DTO TO MODEL

    public static Country createCountryDtoToCountry(CreateCountryDto createCountryDto) {
        Country country = new Country();
        BeanUtils.copyProperties(createCountryDto, country);
        return country;
    }

    public static Cyclist createCyclistDtoToCiclist(CreateCyclistDto createCyclistDto) {
        Cyclist cyclist = new Cyclist();
        BeanUtils.copyProperties(createCyclistDto, cyclist);
        return cyclist;
    }

    public static CyclistTeam createCyclistTeamDtoToCyclistTeam(CreateCyclistTeamDto createCyclistTeamDto) {
        CyclistTeam cyclistTeam = new CyclistTeam();
        BeanUtils.copyProperties(createCyclistTeamDto, cyclistTeam);
        return cyclistTeam;
    }


}
