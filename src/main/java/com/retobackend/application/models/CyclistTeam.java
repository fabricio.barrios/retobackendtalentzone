package com.retobackend.application.models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "cyclistsTeams")
public class CyclistTeam {
    @Id
    private String id;
    private String teamName;
    private String code;
    private Country country;
    private ArrayList<Cyclist> cyclists = new ArrayList<>();
}
