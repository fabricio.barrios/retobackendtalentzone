package com.retobackend.application.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "cyclists")
public class Cyclist {
    @Id
    private String id;
    private String fullName;
    private String numCompetitor;
    private String country;
}
