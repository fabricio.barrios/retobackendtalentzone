package com.retobackend.application.repository;

import com.retobackend.application.models.CyclistTeam;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface CyclistTeamRepository extends ReactiveMongoRepository<CyclistTeam, String> {
    //Mono<CyclistTeamDto> findCyclingTeamByTeamCode(String teamCode); //Método personalizado
}
