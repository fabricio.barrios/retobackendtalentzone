package com.retobackend.application.interfaces;

import com.retobackend.application.dtos.CreateCyclistDto;
import com.retobackend.application.dtos.CyclistDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CyclistInterface {
    Mono<CyclistDto> save(CreateCyclistDto createCyclistDto);
    Flux<CyclistDto> findAllCyclists();

    Flux<CyclistDto> findAllCyclistsByCountry(String country);


    Mono<Void> delete(String id);

    Mono<CyclistDto> update(CyclistDto cyclistDTO, String id);

    Mono<CyclistDto> findCyclistByCompetitorNumber(String competitorNumber);
}
