package com.retobackend.application.interfaces;

import com.retobackend.application.dtos.CountryDto;
import com.retobackend.application.dtos.CreateCountryDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CountryInterface {
    Flux<CountryDto> findAllCountry();

    Mono<CountryDto> save(CreateCountryDto createCountryDto);

    Mono<Void> delete(String id);

    Mono<CountryDto> update(CountryDto countryDTO, String id);

    Mono<CountryDto> countryByCode(String code);
}
