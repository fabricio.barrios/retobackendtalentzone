package com.retobackend.application.interfaces;

import com.retobackend.application.dtos.CreateCyclistTeamDto;
import com.retobackend.application.dtos.CyclistDto;
import com.retobackend.application.dtos.CyclistTeamDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.concurrent.ExecutionException;

public interface CyclistTeamInterface {
    Flux<CyclistTeamDto> findAllCyclingTeams();
    Flux<CyclistDto> findAllCyclistsByTeamCode(String teamCode);
    Mono<CyclistTeamDto> addCyclistToTeamByTeamIDAndIdCylist(String idTeam, String idCyclist);
    Mono<CyclistTeamDto> saveCyclingTeam(CreateCyclistTeamDto createCyclistTeamDto, String idCountry) throws ExecutionException, InterruptedException;
    Mono<Void> delete(String id);

    Mono<CyclistTeamDto> update(CyclistTeamDto cyclistTeamDTO, String id);

    Mono<CyclistTeamDto> findCyclingTeamByTeamCode(String teamCode);
}
