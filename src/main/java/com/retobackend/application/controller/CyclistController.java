package com.retobackend.application.controller;

import com.retobackend.application.dtos.CreateCyclistDto;
import com.retobackend.application.dtos.CyclistDto;
import com.retobackend.application.services.CyclistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
@RequestMapping("/cyclist")
public class CyclistController {

    @Autowired
    CyclistService service;

    @GetMapping("/findAll")
    public ResponseEntity<Flux<CyclistDto>> findAll() {
        return ResponseEntity.ok().body(service.findAllCyclists());
    }

    @PostMapping()
    public ResponseEntity<Mono<CyclistDto>> save(@RequestBody CreateCyclistDto createCyclistDto) {
        return ResponseEntity.ok().body(service.save(createCyclistDto));
    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> delete(@PathVariable("id") String id) {
        return service.delete(id)
                .then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Mono<CyclistDto>> update(@RequestBody CyclistDto cyclistDto,
                                                   @PathVariable("id") String id) {
        return ResponseEntity.ok().body(service.update(cyclistDto, id));
    }

    @GetMapping("/findByNumber/{number}")
    public ResponseEntity<Mono<CyclistDto>> findByCompetitorNumber(@PathVariable("number") String number) {
        return ResponseEntity.ok().body(service.findCyclistByCompetitorNumber(number));
    }
    @GetMapping("/findByCountry/{country}")
    public ResponseEntity<Flux<CyclistDto>> findByCountry(@PathVariable("country") String country) {
        return ResponseEntity.ok().body(service.findAllCyclistsByCountry(country));
    }
}
