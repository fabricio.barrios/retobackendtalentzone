package com.retobackend.application.controller;

import com.retobackend.application.dtos.CountryDto;
import com.retobackend.application.dtos.CreateCountryDto;
import com.retobackend.application.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/country")
public class CountryController {

    @Autowired
    CountryService service;

    @GetMapping("/findAll")
    public ResponseEntity<Flux<CountryDto>> findAll() {
        return ResponseEntity.ok().body(service.findAllCountry());
    }

    @PostMapping()
    public ResponseEntity<Mono<CountryDto>> save(@RequestBody CreateCountryDto createCountryDto) {
        return ResponseEntity.ok().body(service.save(createCountryDto));
    }

    //Cerrar brechas con ResponseEntity
    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> delete(@PathVariable("id") String id) {
        return service.delete(id)
                .then(Mono.just( new ResponseEntity<Void>(HttpStatus.NO_CONTENT)))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Mono<CountryDto>> update(@PathVariable("id") String id,
                                                   @RequestBody CountryDto countryDto) {
        return ResponseEntity
                .ok()
                .body(service.update(countryDto, id));
    }

    @GetMapping("/getByCode/{code}")
    public ResponseEntity<Mono<CountryDto>> getByCode(@PathVariable("code") String code) {
        return ResponseEntity.ok().body(service.countryByCode(code.toUpperCase()));
    }

}
