package com.retobackend.application.controller;

import com.retobackend.application.dtos.CreateCyclistTeamDto;
import com.retobackend.application.dtos.CyclistDto;
import com.retobackend.application.dtos.CyclistTeamDto;
import com.retobackend.application.services.CyclistTeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.concurrent.ExecutionException;

@RestController()
@RequestMapping("/cyclisteam")
public class CyclistTeamController {

    @Autowired
    CyclistTeamService service;

    @PostMapping("/{idCountry}")
    public ResponseEntity<Mono<CyclistTeamDto>> save(@RequestBody() CreateCyclistTeamDto createCyclistTeamDto,
                                                     @PathVariable("idCountry") String idCountry) throws ExecutionException, InterruptedException {
        return ResponseEntity.ok().body(service.saveCyclingTeam(createCyclistTeamDto, idCountry));
    }

    @GetMapping("/findAll")
    public ResponseEntity<Flux<CyclistTeamDto>> findAll() {
        return ResponseEntity.ok().body(service.findAllCyclingTeams());
    }

    @GetMapping("/findByTeamCode/{idCode}")
    public ResponseEntity<Mono<CyclistTeamDto>> findCyclistTeamByCode(@PathVariable("idCode") String idCode) {
        return ResponseEntity.ok().body(service.findCyclingTeamByTeamCode(idCode));
    }

    @GetMapping("/allCyclistByTeamCode/{codeTeam}")
    public ResponseEntity<Flux<CyclistDto>> findAllCyclistsByTeamCode(@PathVariable("codeTeam") String codeTeam) {
        return ResponseEntity.ok().body(service.findAllCyclistsByTeamCode(codeTeam));
    }

    @PutMapping("/addCyclist/{idTeam}/{idCyclist}")
    public ResponseEntity<Mono<CyclistTeamDto>> addCyclistToTeamByTeamIdAndCyclistId(@PathVariable("idTeam") String idTeam,
                                                                                     @PathVariable("idCyclist") String idCyclist) {
        return ResponseEntity.ok().body(service.addCyclistToTeamByTeamIDAndIdCylist(idTeam, idCyclist));
    }

    @DeleteMapping("/{id}")
    public Mono<Void> delete(@PathVariable("id") String id) {
        return service.delete(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Mono<CyclistTeamDto>> update(@PathVariable("id") String id,
                                                       @RequestBody() CyclistTeamDto cyclistTeamDto) {
        return ResponseEntity.ok().body(service.update(cyclistTeamDto, id));
    }
}
