package com.retobackend.application.services;

import com.retobackend.application.dtos.CountryDto;
import com.retobackend.application.dtos.CreateCountryDto;
import com.retobackend.application.interfaces.CountryInterface;
import com.retobackend.application.models.Country;
import com.retobackend.application.repository.CountryRepository;
import com.retobackend.application.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CountryService implements CountryInterface {

    @Autowired
    CountryRepository repository;

    @Override
    public Flux<CountryDto> findAllCountry() {
        return repository.findAll().map(AppUtils::countryToCountryDto);
    }

    @Override
    public Mono<CountryDto> save(CreateCountryDto createCountryDto) {
        Country country = AppUtils.createCountryDtoToCountry(createCountryDto);
        return repository.save(country).map(AppUtils::countryToCountryDto);
    }

    @Override
    public Mono<Void> delete(String id) {
        return repository
                .findById(id)//Validation
                .flatMap(country -> repository.deleteById(country.getId()))
                .switchIfEmpty(Mono.empty());
    }

    @Override
    public Mono<CountryDto> update(CountryDto countryDTO, String id) {
        return repository.findById(id).flatMap(country -> {
            Country countryUpdated = AppUtils.countryDtoToCountry(countryDTO);
            countryUpdated.setId(id);
            return repository.save(countryUpdated);
        }).map(AppUtils::countryToCountryDto);
    }

    @Override
    public Mono<CountryDto> countryByCode(String code) {
        return repository
                        .findAll()
                        .filter(country -> country.getCode().equals(code))
                        .map(AppUtils::countryToCountryDto)
                        .next();
    }
}
