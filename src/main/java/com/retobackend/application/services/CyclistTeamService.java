package com.retobackend.application.services;

import com.retobackend.application.dtos.CreateCyclistTeamDto;
import com.retobackend.application.dtos.CyclistDto;
import com.retobackend.application.dtos.CyclistTeamDto;
import com.retobackend.application.interfaces.CyclistTeamInterface;
import com.retobackend.application.models.Country;
import com.retobackend.application.models.CyclistTeam;
import com.retobackend.application.repository.CountryRepository;
import com.retobackend.application.repository.CyclistRepository;
import com.retobackend.application.repository.CyclistTeamRepository;
import com.retobackend.application.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.concurrent.ExecutionException;

@Service
public class CyclistTeamService implements CyclistTeamInterface {

    @Autowired
    CyclistTeamRepository repository;

    @Autowired
    CountryRepository countryRepository;

    @Autowired
    CyclistRepository cyclistRepository;

    @Override
    public Flux<CyclistTeamDto> findAllCyclingTeams() {
        return repository.findAll().map(AppUtils::cyclisTeamToCyclistTeamDto);
    }

    @Override
    public Mono<CyclistTeamDto> findCyclingTeamByTeamCode(String teamCode) {
        return repository.findAll()
                .filter(cyclistTeam -> cyclistTeam.getCode().equals(teamCode))
                .map(AppUtils::cyclisTeamToCyclistTeamDto)
                .next();
    }

    @Override
    public Flux<CyclistDto> findAllCyclistsByTeamCode(String teamCode) {
        return repository.findAll()
                .filter(cyclistTeam -> cyclistTeam.getCode().equals(teamCode))
                .flatMapIterable(CyclistTeam::getCyclists)
                .map(AppUtils::cyclistToCyclistDto);
    }

    @Override
    public Mono<CyclistTeamDto> addCyclistToTeamByTeamIDAndIdCylist(String idTeam, String idCyclist) {
        return repository.findById(idTeam)
                .flatMap(cyclistTeam -> {
                    if (cyclistTeam.getCyclists().size() < 8) {
                        return cyclistRepository.findById(idCyclist)
                                .flatMap(cyclist -> {
                                    cyclistTeam.getCyclists().add(cyclist);
                                    return repository.save(cyclistTeam);
                                });
                    }
                    return Mono.just(cyclistTeam);
                }).map(AppUtils::cyclisTeamToCyclistTeamDto);
    }

    @Override
    public Mono<CyclistTeamDto> saveCyclingTeam(CreateCyclistTeamDto createCyclistTeamDto, String idCountry) throws ExecutionException, InterruptedException {
        CyclistTeam cyclistTeam = AppUtils.createCyclistTeamDtoToCyclistTeam(createCyclistTeamDto);
        Country country = countryRepository.findById(idCountry).toFuture().get();
        cyclistTeam.setCountry(country);
        return repository.save(cyclistTeam).map(AppUtils::cyclisTeamToCyclistTeamDto);
    }

    @Override
    public Mono<Void> delete(String id) {
        return repository
                .findById(id)
                .flatMap(cyclistTeam -> repository.deleteById(cyclistTeam.getId()))
                .switchIfEmpty(Mono.empty());
    }

    @Override
    public Mono<CyclistTeamDto> update(CyclistTeamDto cyclistTeamDTO, String id) {
        return repository.findById(id)
                .flatMap(cyclistTeam -> {
                    CyclistTeam cyclistTeam1 = AppUtils.cyclistTeamDtoToCyclistTeam(cyclistTeamDTO);
                    cyclistTeam1.setId(id);
                    return repository.save(cyclistTeam1);
                })
                .map(AppUtils::cyclisTeamToCyclistTeamDto)
                .switchIfEmpty(Mono.empty());
    }


}
