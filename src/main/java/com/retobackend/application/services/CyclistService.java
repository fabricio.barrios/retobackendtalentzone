package com.retobackend.application.services;

import com.retobackend.application.dtos.CreateCyclistDto;
import com.retobackend.application.dtos.CyclistDto;
import com.retobackend.application.interfaces.CyclistInterface;
import com.retobackend.application.models.Cyclist;
import com.retobackend.application.repository.CyclistRepository;
import com.retobackend.application.utils.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CyclistService implements CyclistInterface {

    @Autowired
    CyclistRepository repository;

    @Override
    public Mono<CyclistDto> save(CreateCyclistDto createCyclistDto) {
        Cyclist cyclist = AppUtils.createCyclistDtoToCiclist(createCyclistDto);
        return repository.save(cyclist)
                .map(AppUtils::cyclistToCyclistDto);
    }

    @Override
    public Flux<CyclistDto> findAllCyclists() {
        return repository.findAll()
                .map(AppUtils::cyclistToCyclistDto);
    }

    @Override
    public Flux<CyclistDto> findAllCyclistsByCountry(String country) {
        return findAllCyclists()
                .filter(cyclistDto -> cyclistDto.getCountry().toLowerCase().equals(country.toLowerCase()));
    }

    @Override
    public Mono<Void> delete(String id) {
        return repository.deleteById(id);
    }

    @Override
    public Mono<CyclistDto> update(CyclistDto cyclistDTO, String id) {
        return repository.findById(id).flatMap(cyclist -> {
            Cyclist updatedCyclist = AppUtils.cyclistDtoToCyclist(cyclistDTO);
            updatedCyclist.setId(id);
            return repository.save(updatedCyclist);
        }).map(AppUtils::cyclistToCyclistDto);
    }

    @Override
    public Mono<CyclistDto> findCyclistByCompetitorNumber(String competitorNumber) {
        return findAllCyclists()
                .filter(cyclistDto -> cyclistDto.getNumCompetitor()
                        .equals(competitorNumber))
                .next();
    }
}
