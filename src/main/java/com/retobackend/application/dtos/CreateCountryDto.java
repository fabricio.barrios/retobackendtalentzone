package com.retobackend.application.dtos;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateCountryDto {
    @NonNull
    String name;
    @NonNull
    String code;
}
