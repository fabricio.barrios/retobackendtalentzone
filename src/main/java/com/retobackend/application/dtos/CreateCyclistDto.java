package com.retobackend.application.dtos;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateCyclistDto {
    @NonNull
    private String fullName;
    @NonNull
    private String numCompetitor;
    private String country;
}
