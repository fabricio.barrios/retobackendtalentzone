package com.retobackend.application.dtos;

import lombok.*;
import org.springframework.data.annotation.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CountryDto {
    @Id
    String id;
    String code;
    String name;
}
