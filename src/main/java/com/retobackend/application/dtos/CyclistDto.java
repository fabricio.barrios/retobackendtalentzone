package com.retobackend.application.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CyclistDto {
    @Id
    private String id;
    private String fullName;
    private String numCompetitor;
    private String country;
}
