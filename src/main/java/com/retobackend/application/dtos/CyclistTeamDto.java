package com.retobackend.application.dtos;

import com.retobackend.application.models.Country;
import com.retobackend.application.models.Cyclist;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CyclistTeamDto {
    @Id
    private String id;
    private String teamName;
    private String code;
    private Country country;
    private ArrayList<Cyclist> cyclists = new ArrayList<>();
}
