package com.retobackend.application.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.retobackend.application.models.Country;
import lombok.*;

import java.util.ArrayList;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateCyclistTeamDto {
    @NonNull
    private String teamName;
    @NonNull
    private String code;
    @JsonIgnore
    private Country country;
    @JsonIgnore
    private ArrayList<CyclistDto> cyclists;
}
